# Desafio DevOps New Way

O desafio tem como objetivo entender o seu conhecimento sobre o mundo de micro-serviços, infraestrutura, containers, observabilidade e AWS, documente e explique suas escolhas:

- Criar uma conta Free tier na AWS (Atenção com os serviços que serão utilizados durante o teste, para não haver cobranças desnecessárias)
- Todo o processo precisa seguir documentação, desenho da arquitetura e explicação de código, tudo comitado no Gitlab ;
- Utilize o processo de CI/CD do próprio Gitlab-CI ou de sua preferência;
- Configure uma API Hello World exponto-a com HTTPS com uma linguagem de sua preferência;
    - Utilize domínio gratuito para o HTTPS;
    - Utilize certificado Letsencrypt;
    - Utilizar o Nginx para proxy pass;
- Toda infraestrutura e serviço devem ser configurados utilizando o Terraform commitado no Gitlab-CI;
    - Utilize as melhores práticas no uso do Terraform;
    - Se possível configure no próprio CI/CD do Terraform o teste de código da sua infraestrutura;
- Observabilidade utilizando Grafana, Zabbix ou Prometheus;
- Utilizar em todos os serviços propostos utilizando containeres Docker
- Utilizar ferramentas de automação de sua preferência (Ansible, Rundeck, Jenkins etc..)
- Todos os serviços devem ser configurados em Servidores Linux;

## Ao final do desafio gostaríamos:

- Criar um usuário na AWS com permissão apenas de leitura nos seviços utilizados na AWS;
- Acesso ao código do Gitlab de todo o ambiente criado e documentações;
- Não fique limitado apenas ao que o desafio solicita!
- Entregue o que souber e o que conseguir, porém é levado em consideração toda documentação explicativa caso não consiga.
- Nos surpreenda!

## Após a avaliação:

- Não se esqueça de deletar os serviços ou encerrar a conta Free tier da AWS;

